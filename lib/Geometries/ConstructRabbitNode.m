function [x,X] = ConstructRabbitNode(model,movingModel)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Coarse grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sides of mouse node
xT = ConstructRabbitNodeTop(model,model.nFTop);

% Base points
xB = ConstructRabbitNodeBase(model,model.nFBot);

% Patch points for cilia
if model.p_f ~= 1
    % Add patch near cilia
    patchFunc = @(xx,yy) - model.lB * ...
        (1-(yy/model.lT).^2 - (xx/model.lT./(1-yy/2/model.lT)).^2).^(0.5);
    
    for ii = 1 : movingModel.nCilia
        % Remove base points close to cilia
        x0 = movingModel.model(ii).x0;
        
        x0(3) = - movingModel.model(ii).lB ...
            * sqrt( -x0(2)^2/movingModel.model(ii).lT^2 ...
            - x0(1)^2/movingModel.model(ii).lT^2 ...
            / ((1 - x0(2)/2/movingModel.model(ii).lT)^2) + 1);
        
        xB = RemoveClosePoints(xB,x0,model.pL);
        
        % Add circular patch about cilia beat
        xP = GenerateDiscretisationPatch(model.pL,model.p_f,x0,patchFunc);
        
        % Integrate patch into base
        xB = MergeVectorGrids(xB,xP);
    end
end

% Merge grids
x = MergeVectorGrids(xT,xB);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fine grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sides of mouse node
XT = ConstructRabbitNodeTop(model,model.nQTop);

% Base points
XB = ConstructRabbitNodeBase(model,model.nQBot);

if model.p_f ~= 1
    % Add patch near cilia
    for ii = 1 : movingModel.nCilia
        % Remove base points close to cilia
        X0 = movingModel.model(ii).x0;
        
        X0(3) = - movingModel.model(ii).lB * sqrt( -X0(2)^2/movingModel.model(ii).lT^2 ...
            - X0(1)^2/movingModel.model(ii).lT^2 / ((1 - X0(2)/2/movingModel.model(ii).lT)^2) + 1);
        
        XB = RemoveClosePoints(XB,X0,model.pL);
        
        % Add circular patch about cilia beat
        XP = GenerateDiscretisationPatch(model.pL,model.p_q,X0,patchFunc);
        
        % Integrate patch into base
        XB = MergeVectorGrids(XB,XP);
    end
end

% Merge grids
X = MergeVectorGrids(XT,XB);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create overlapped grids
X = MergeVectorDiscr_NoDuplicates(X,x);

end