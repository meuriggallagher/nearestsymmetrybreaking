%PLACECILIUM Places cilium at specified location
%
function [x,v,R,NNmoving] = PlaceCilium(x,v,Rold,t,model,runType,varargin)

% Generate cilium
[xC,vC,Rnew] = CiliumModelRod(t,model,runType);

% Calculate point on boundary to place cilium
switch model.animal
    case 'mouse'
        x10 = model.x0(1);
        x20 = model.x0(2);
        x30 = - model.lB * sqrt( -x20^2/model.lT^2 ...
            - x10^2/model.lT^2 / ((1 - x20/2/model.lT)^2) + 1);
    case 'flatMouse'
        x10 = model.x0(1);
        x20 = model.x0(2);
        x30 = 0;
    case 'rabbit'
        x10 = model.x0(1);
        x20 = model.x0(2);
        x30 = GetBoundaryPoint([x10(:),x20(:),NaN*x10(:)],model);
    otherwise
        error('Animal node boundary not specified')
end

% Calculate normal to plane at model.x0
n = NormalToBoundary([x10,x20,x30],model);

switch runType
    case 'hybridStokeslets'
        % Rotate cilium so normal to plane
        if ~isequal(n,[0,0,1])
            [xC,vC,Rnew] = RotateCilium(xC,vC,Rnew,n);
        end
        
        R = [Rold, repmat(Rnew,1,model.nS)];
        
        % Translate cilium to [x10,x20,x30]
        xC = xC + [x10,x20,x30];
        
        % Append cilium
        x = MergeVectorGrids(x,xC);
        v = MergeVectorGrids(v,vC);
        
        % No moving NN matrix required
        NNmoving = [];
        
    case 'nearestNeighbour'
        % Rotate cilium so normal to plane
        if ~isequal(n,[0,0,1])
            [xC,vC,XC] = RotateCilium(xC,vC,Rnew,n);
        end
        
        % Translate cilium to [x10,x20,x30]
        xC = xC + [x10,x20,x30];
        XC = XC + [x10,x20,x30];
        
        % Append cilium
        x = MergeVectorGrids(x,xC);        
        R = MergeVectorGrids(Rold,XC);
        v = MergeVectorGrids(v,vC);
        
        % Nearest neighbour matrix if required
        if ~isempty(varargin)
            NNold = varargin{1};
            NNnew = NearestNeighbourMatrix(XC(:),xC(:));
            
            % Merge NN Matrices
            NNmoving = MergeNNMatrices(NNold,NNnew);
            
        else
            NNmoving = [];
        end
end
end