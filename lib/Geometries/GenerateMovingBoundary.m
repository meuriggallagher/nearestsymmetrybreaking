%GENERATEMOVINGBOUNDARY
%
function [x,v,R,NNMov] = GenerateMovingBoundary(t,model,varargin)

nCilia = model.nCilia;
runType = model.runType;
model = model.model;

switch runType
    case 'hybridStokeslets'
        x = []; v = []; R = [];
        for ii = 1:nCilia
            [x,v,R] = PlaceCilium(x,v,R,t,model(ii),runType);
        end
        
        % No NN matrix required
        NNMov = [];
        
    case 'nearestNeighbour'
        if ~isempty(varargin)
            x = []; v = []; R = []; NNMov = [];
            for ii = 1:nCilia
                [x,v,R,NNMov] = PlaceCilium(x,v,R,t,model(ii), ...
                    runType,NNMov);
            end
        else
            x = []; v = []; R = [];
            for ii = 1:nCilia
                [x,v,R] = PlaceCilium(x,v,R,t,model(ii), ...
                    runType);
            end
        end
end
end