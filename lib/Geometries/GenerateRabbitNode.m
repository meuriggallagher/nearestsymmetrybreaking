function [stationaryBoundary,movingBoundary] = GenerateRabbitNode(h, ...
    nC,cx0,blockSize,runType)

%% Create moving boundary model
movingBoundary = struct('fn',@GenerateMovingBoundary, ...
    'model',[],'nCilia',nC,'runType',runType);

% First cilium
movingBoundary.model(1).animal = 'rabbit';
movingBoundary.model(1).l = 1; % lengths scaled in multiples of cilium lengths
movingBoundary.model(1).lxC = 7.07 / 2;        % Cilia region semi-minor axis
movingBoundary.model(1).lyC = 3 * movingBoundary.model(1).lxC;   % Cilia region semi-major axis
movingBoundary.model(1).lzC = 1.347;

movingBoundary.model(1).Psi = 55 / 180 * pi; % Semi-cone angle
movingBoundary.model(1).Th = 40 / 180 * pi; % Posterior tilt
movingBoundary.model(1).bFreq = 10; % Beat frequency Hz NOT USED IN CODE
movingBoundary.model(1).phaseShift = 0;

movingBoundary.model(1).nS = h(7); % Number of points along cilium

% Initialise cilia
for ii = 1 : movingBoundary.nCilia
    movingBoundary.model(ii) = movingBoundary.model(1);
    movingBoundary.model(ii).x0 = cx0(ii,:);
end


%% Create stationary boundary
stationaryBoundary = struct('fn',@ConstructRabbitNode, ...
    'model',[],'x',[],'X',[],'NN',[],'runType',runType);

stationaryBoundary.model.animal = movingBoundary.model.animal;

% Node geometry parameters
stationaryBoundary.model.lxC = movingBoundary.model(1).lxC;        % Cilia region semi-minor axis
stationaryBoundary.model.lyC = movingBoundary.model(1).lyC;   % Cilia region semi-major axis
stationaryBoundary.model.lzC = movingBoundary.model(1).lzC;

stationaryBoundary.model.lxB = 7.58 / 2;        % Base semi-minor axis
stationaryBoundary.model.lyB = 3 * stationaryBoundary.model.lxB;   % Base semi-major axis
stationaryBoundary.model.lzB = 0;

stationaryBoundary.model.lxT = 13.47 / 2;       % Top semi-minor axis
stationaryBoundary.model.lyT = 2.5 * stationaryBoundary.model.lxT; % Top semi-major axis
stationaryBoundary.model.lzT = 2.357;

% Main discretisation
stationaryBoundary.model.nFTop = h(1);
stationaryBoundary.model.nQTop = h(2);
stationaryBoundary.model.nFBot = h(3);
stationaryBoundary.model.nQBot = h(4);

% Patch discretisation
stationaryBoundary.model.pL = movingBoundary.model(1).l;
stationaryBoundary.model.p_f = h(5);
stationaryBoundary.model.p_q = h(6);

% Generate points
[stationaryBoundary.x,stationaryBoundary.X] = ConstructRabbitNode( ...
    stationaryBoundary.model,movingBoundary);

%% Calculate Nearest-Neighbour matrix
switch runType
    case 'hybridStokeslets'
        stationaryBoundary.NN = NearestNeighbourMatrix(stationaryBoundary.X, ...
            stationaryBoundary.x,blockSize);
    case 'nearestNeighbour'
        NNstat = NearestNeighbourMatrix(stationaryBoundary.X, ...
            stationaryBoundary.x,blockSize);
        
        [~,~,~,NNmov] = movingBoundary.fn(0,movingBoundary,1);
        
        stationaryBoundary.NN = MergeNNMatrices(NNstat,NNmov);
end

end