% Solves time-varying nearest-neighbour problem
%
% input:
%   x00        Initial position vector
%   b10, b20   Initial basis vectors (third basis vector is calculated via
%              cross product)
%   tRange     time range over which to calculate trajectory
%   swimmer    Structure describing how to calculate swimmer shape and
%              kinematics
%   boundary   Structure describing boundary
%   epsilon    regularisation parameter
%   domain     switch for use of image systems
%              'i' = use infinite fluid (usual)
%              'h' = use half space images (Ainley/Blakelet)
%   blockSize  memory size for stokeslet matrix block assembly in GB.
%              0.2 is a safe choice.
%   varargin   if varargin{1}='f' then includes force components
%
function [t,z] = SolveTimeDependentProblem(tRange, ...
    swimmer,boundary,epsilon,domain,blockSize,solveFlag,problemFlag, ...
    NNMatrices)


switch problemFlag
    case 'swimming'
        
        nSw = length(swimmer);
        
        % Calculate DOF
        DOF = 0;
        for iSw = 1 : nSw
            [xForce,~,~] = swimmer{iSw}.fn(0,swimmer{iSw}.model);
            DOF = DOF + length(xForce);
        end
        
        if ~isempty(boundary)
            [xQuad,~] = boundary.fn(boundary.model);
            DOF = DOF + length(xQuad);
        end
        
        fprintf(['Solving time-dependent swimming ', ...
            'problem with %i DOF...\n'],DOF)
        
        z0 = zeros(9*nSw + DOF, 1);
        for iSw = 1 : nSw
            z0(iSw)         = swimmer{iSw}.x0(1);
            z0(nSw   + iSw) = swimmer{iSw}.x0(2);
            z0(2*nSw + iSw) = swimmer{iSw}.x0(3);
            z0(3*nSw + iSw) = swimmer{iSw}.b10(1);
            z0(4*nSw + iSw) = swimmer{iSw}.b10(2);
            z0(5*nSw + iSw) = swimmer{iSw}.b10(3);
            z0(6*nSw + iSw) = swimmer{iSw}.b20(1);
            z0(7*nSw + iSw) = swimmer{iSw}.b20(2);
            z0(8*nSw + iSw) = swimmer{iSw}.b20(3);
        end
        
        switch solveFlag
            case 'ode45'
                [t,z]=ode45(@(t,z) SolveMatrixProblem(z,swimmer, ...
                    boundary,t,epsilon,domain,blockSize,problemFlag, ...
                    NNMatrices),tRange,z0);
            case 'ode113'
                [t,z]=ode113(@(t,z) SolveMatrixProblem(z,swimmer, ...
                    boundary,t,epsilon,domain,blockSize,problemFlag, ...
                    NNMatrices),tRange,z0);
            otherwise
                error('Solve flag not known')
        end
        
    otherwise
        error('Problem type not known')
end

end
