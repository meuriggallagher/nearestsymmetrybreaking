% solves force-free swimming problem for translational and angular velocity U, Om
% in the presence of a stationary rigid boundary (e.g. finite plane wall)
%
% input: z       - position/orientation of swimmer
%                  dz(1:3) = x0      - origin of swimmer
%                  dz(4:6) = b1      - first basis vector of swimmer frame
%                  dz(7:9) = b2      - second basis vector of swimmer frame
%        swimmer - structure describing how to construct swimmer
%        xb      - discretisation of stationary boundary - force points
%        Xb      - discretisation of stationary boundary - quadrature points
%        t       - time (scalar)
%        varargin - Nearest neighbour matrices:
%           varargin{1} - NN swimmer
%                       - NN boundary
%                       - full NN
%
% variables: U      - translational velocity
%            Om     - angular velocity
%
% output: dz        - rate of change of position-orientation (x,b1,b2)
%                     first three compts are dx0/dt,
%                     next three are db1/dt,
%                     next three are db2/dt
%                   - then includes force components
%
function dz = SolveMatrixProblem(z,swimmer,boundary,t,epsilon, ...
    domain, blockSize,problemFlag,NNMatrices)

t

%% Choose, construct, and solve  matrix problem
switch problemFlag
    case 'swimming'
        NN = NNMatrices{1};
        NNSw = NNMatrices{2};
        
        dz = SwimmingProblem(z,swimmer,boundary,t,epsilon, ...
            domain, blockSize,NN,NNSw);
    otherwise
        error('Problem construction not found')
end


end
