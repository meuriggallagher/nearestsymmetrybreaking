% Constructs the force-free swimming problem for translational and angular 
% velocity U, Om, in the presence of a stationary rigid boundary 
% (e.g. finite plane wall)
%
% input: z{s}     - position/orientation of swimmer{s}
%                   z{s}(1:3) = x0   - origin of swimmer
%                   z{s}(4:6) = b1   - first basis vector of swimmer frame
%                   z{s}(7:9) = b2   - second basis vector of swimmer frame
%        swimmer  - structure describing how to construct swimmer
%        t        - time (scalar)
%        varargin - Nearest neighbour matrices:
%        varargin{1} - NN swimmer
%                    - NN boundary
%                    - full NN
%
%
% output: A, b     - matrix system A * x = b
%                     	where x comprises forces at all points x1 ; x2 ; x3
%                     	followed by velocities U in same order
%                       followed by angular velocities Om in same order
function dz = SwimmingProblem(z,swimmer,boundary,t,epsilon, ...
    domain, blockSize, NN, NNsw)
%% Establish number of swimmers
nSw = length(swimmer);

%% Obtain swimmer points
x0 = cell(nSw,1);
b1 = cell(nSw,1);
b2 = cell(nSw,1);
B = cell(nSw,1);
for iSw = 1 : nSw
    % Swimmer position
    x0{iSw} = z(iSw : nSw : iSw + 2 * nSw); 

    % Swimmer body frame
    b1{iSw} = z(3 * nSw + iSw : nSw : iSw + 5 * nSw);
    b2{iSw} = z(6 * nSw + iSw : nSw : iSw + 8 * nSw);
    b3 = cross(b1{iSw},b2{iSw});
    B{iSw} = [b1{iSw}(:) b2{iSw}(:) b3(:)];

end

xForceSw = cell(nSw,1); xForceSwRotate = cell(nSw,1);
vForceSw = cell(nSw,1);
xQuadSw = cell(nSw,1);
for iSw = 1 : nSw
    [xForceSw{iSw},vForceSw{iSw},xQuadSw{iSw}] = ...
        swimmer{iSw}.fn(t,swimmer{iSw}.model);
    
    xForceSwRotate{iSw} = ApplyRotationMatrix(B{iSw},xForceSw{iSw});
    xForceSw{iSw} = TranslatePoints(xForceSwRotate{iSw},x0{iSw});
    
    vForceSw{iSw} = ApplyRotationMatrix(B{iSw},vForceSw{iSw});
    
    xQuadSw{iSw} = ApplyRotationMatrix(B{iSw},xQuadSw{iSw});
    xQuadSw{iSw} = TranslatePoints(xQuadSw{iSw},x0{iSw});
end

% Get indices of swimmer points
indSw = NaN(nSw+1,1);
indSw(1) = 1;
for ii = 1 : nSw
    indSw(ii+1) = indSw(ii) + length(xForceSw{ii})/3;
end

% Merge points together
xForce = xForceSw{1};
vForce = vForceSw{1};
xQuad = xQuadSw{1};
if nSw > 1
    for iSw = 2 : nSw
        xForce = MergeVectorGrids(xForce, xForceSw{iSw});
        vForce = MergeVectorGrids(vForce, vForceSw{iSw});
        xQuad = MergeVectorGrids(xQuad, xQuadSw{iSw});
    end
end

%% Extract boundary points
if ~isempty(boundary)
    [xForceBnd,xQuadBnd] = boundary.fn(boundary.model);
    vBnd = 0 * xForceBnd;
else
    xForceBnd = [];
    xQuadBnd = [];
    vBnd = [];
end

% Merge swimmer and boundary points
x = MergeVectorGrids(xForce, xForceBnd);
X = MergeVectorGrids(xQuad, xQuadBnd);
v = MergeVectorGrids(vForce, vBnd);

%% Assemble matrix system - mobility problem
NSw = length(xForce)/3;
QSw = length(xQuad)/3;
NBnd = length(xForceBnd)/3;
N = NSw + NBnd;

% Assemble stokeslet matrix
[AS,~]=AssembleStokesletMatrix(x,X,x,epsilon,domain,blockSize,NN);

% component of velocity due to translational velocity of swimmers; 
%   zero velocity of boundary
au = zeros(NSw+NBnd,nSw);
for n = 1 : nSw
    au(indSw(n):indSw(n+1)-1,n) = -ones(indSw(n+1)-indSw(n),1);
end
AU = kron(eye(3),au);

% force summation - only on swimmer
af = zeros(nSw, N);
for n= 1 : nSw
    af(n,indSw(n):indSw(n+1)-1) = sum(NNsw(1:QSw,indSw(n):indSw(n+1)-1),1);
end
AF = kron(eye(3),af);

% component of velocity due to rotation of swimmer about x0; 
%   zero velocity of boundary
Ze  = zeros(N, nSw);
x1m = zeros(N, nSw);
x2m = zeros(N, nSw);
x3m = zeros(N, nSw);
for n = 1 : nSw
    [x1,x2,x3]=ExtractComponents(xForceSwRotate{n});
    x1m(indSw(n):indSw(n+1)-1,n) = x1;  
    x2m(indSw(n):indSw(n+1)-1,n) = x2;
    x3m(indSw(n):indSw(n+1)-1,n) = x3;
end
AOm = [ Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];

%
Ze  = zeros(nSw,N);
x1m = zeros(nSw,N);
x2m = zeros(nSw,N);
x3m = zeros(nSw,N);
[x1,x2,x3]=ExtractComponents(xQuad'*NNsw); % moment summation
for n=1:nSw
    x1m(n,indSw(n):indSw(n+1)-1)=x1(indSw(n):indSw(n+1)-1);
    x2m(n,indSw(n):indSw(n+1)-1)=x2(indSw(n):indSw(n+1)-1);
    x3m(n,indSw(n):indSw(n+1)-1)=x3(indSw(n):indSw(n+1)-1);
end
AM = [Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];

A=[ AS, AU, AOm            ; ...
    AF, zeros(3*nSw,6*nSw) ; ...
    AM, zeros(3*nSw,6*nSw)];

%% Assemble RHS of problem
b = [v ; zeros(6*nSw,1)];

%% Solve problem
sol = A \ b;

%% Extract solution
% Initialise output
dz = NaN(9*nSw+3*N, 1);

% Velocities
U = sol(3*N+1       : 3*N+3*nSw);
dz(1 : 3*nSw, 1)  = U;

% Angular velocities
Om= sol(3*N+3*nSw+1 : 3*N+6*nSw);
for n = 1 : nSw
    om = Om(n : nSw : n + 2*nSw);
    dz(3*nSw+n:nSw:n+5*nSw,1)  = cross(om, b1{n});
    dz(6*nSw+n:nSw:n+8*nSw,1)  = cross(om, b2{n});
end

% Forces
f = sol(1 : 3*N);
dz(9*nSw+1:9*nSw+3*N,1) = f;

end