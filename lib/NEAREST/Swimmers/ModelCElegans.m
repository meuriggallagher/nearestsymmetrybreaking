function [x,v,X] = ModelCElegans(t,model)
% generates discretisation of a model C. elegans, griddedInterpolant body
%
% t - time
% model.f - number of force points along body midpoint
% model.q - number of quadrature points along body midpoint
%
% model.L0 - length of body
% model.r0 - max body radius
% model.h0 - length of head
% model.t0 - length of tail section
%
% model.F  - F{1} is x-interpolant, F{2} is y-interpolant

%% Coarse grid - position and velocity
% Get midpoints along body
s = linspace(0,1,model.fR);
dt=0.001;
tt=[t-dt/2  t  t+dt/2];
[sg,tg] = ndgrid(s,tt);

theta = NaN(size(sg));
for ii = 1 : size(theta,2)
    dum = model.tangentAngleFn(sg(:,ii),mod(tt(ii),1));
    theta(:,ii) = dum(:);
end

% theta = model.tangentAngleFn(sg,mod(tg,1));
[xMid,yMid] = CalcxyFromPlanarInterp(sg,mod(tg,1),model.F);

% Radius along body
r = GetCElegansRadius(s,model);

% Angles around body
th = linspace(0,2*pi,model.fTh+1); th = th(2:end);

% Create circles at each r oriented with s
x2 = r(2:end-1) * cos(th(:)');
x3 = r(2:end-1) * sin(th(:)');

x2 = [0 ; x2(:); 0];
x3 = [0 ; x3(:); 0];
x1 = 0 * x2;

x = cell(3,1);
for ii = 1 : 3
    % Rotate to be in lab frame
    th = [theta(1,ii); repmat(theta(2:end-1,ii),model.fTh,1);theta(end,ii)];
    xt1 = cos(th(:)).*x1(:) - sin(th(:)).*x2(:);
    xt2 = sin(th(:)).*x1(:) + cos(th(:)).*x2(:);
    
    % Move along s
    xt1 = xt1 + [xMid(1,ii) ; repmat(xMid(2:end-1,ii),model.fTh,1) ; xMid(end,ii)];
    xt2 = xt2 + [yMid(1,ii) ; repmat(yMid(2:end-1,ii),model.fTh,1) ; yMid(end,ii)];
    
    x{ii} = [xt1(:), xt2(:), x3(:)];
end

% Velocities
v1=(x{3}(:,1)-x{1}(:,1))/dt;
v2=(x{3}(:,2)-x{1}(:,2))/dt;
v3=0*v1;

% Remove points which are too close together
xDum = [x{2}(:,1),x{2}(:,2),x{2}(:,3)];
vDum = [v1(:), v2(:), v3(:)];

dist = (xDum(:,1) - xDum(:,1)').^2 + (xDum(:,2) - xDum(:,2)').^2 ...
    + (xDum(:,3) - xDum(:,3)').^2;
dist(dist == 0) = inf;

while min(dist(:)) < model.eps^2
    [~,ii] = min(dist(:));
    [~,jj] = ind2sub(size(dist),ii);
    
    % Delete point jj
    xDum(jj,:) = [];
    vDum(jj,:) = [];
    
    dist = (xDum(:,1) - xDum(:,1)').^2 + (xDum(:,2) - xDum(:,2)').^2 ...
    + (xDum(:,3) - xDum(:,3)').^2;
    dist(dist == 0) = inf;
end

x = xDum(:);
v = vDum(:);

%% Fine grid
% Get midpoints along body
S = linspace(0,1,model.qR);
[Sg,Tg] = ndgrid(S,t);
Theta = NaN(size(Sg));
for ii = 1 : size(Theta,2)
    dum = model.tangentAngleFn(Sg(:,ii),mod(tt(ii),1));
    Theta(:,ii) = dum(:);
end

[XMid,YMid] = CalcxyFromPlanarInterp(Sg,mod(Tg,1),model.F);

% Radius along body
R = GetCElegansRadius(S,model);

% Angles around body
Th = linspace(0,2*pi,model.qTh+1); Th = Th(2:end);

% Create circles at each r oriented with s
X2 = R(2:end-1) * cos(Th(:)');
X3 = R(2:end-1) * sin(Th(:)');

X2 = [0 ; X2(:); 0];
X3 = [0 ; X3(:); 0];
X1 = 0 * X2;

% Rotate to be in lab frame
Th = [Theta(1); repmat(Theta(2:end-1),model.qTh,1); Theta(end)];
Xt1 = cos(Th(:)).*X1(:) - sin(Th(:)).*X2(:);
Xt2 = sin(Th(:)).*X1(:) + cos(Th(:)).*X2(:);
    
% Move along s
Xt1 = Xt1 + [XMid(1) ; repmat(XMid(2:end-1),model.qTh,1) ; XMid(end)];
Xt2 = Xt2 + [YMid(1) ; repmat(YMid(2:end-1),model.qTh,1) ; YMid(end)];

% Ensure non-overlapping discretisations
X = [Xt1(:),Xt2(:),X3];
X(ismember(X,xDum,'rows'),:)=[];

X = X(:);

end