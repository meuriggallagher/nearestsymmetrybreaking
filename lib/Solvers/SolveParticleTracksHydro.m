function [t,p] = SolveParticleTracksHydro(fourierCoefs,movingBoundary, ...
    stationaryBoundary,particleModel,nBeats,epsilon,domain,blockSize,varargin)

global outputCounter

if isempty(varargin)
    % Indices for parallelisation
    p0 = [particleModel.p0(:,1) ; ...
        particleModel.p0(:,2) ; ...
        particleModel.p0(:,3)];
    
    outputCounter = 1;
    
    [t,p] = ode45(@(T,P) EvaluateVelocityFromFourier(stationaryBoundary, ...
        movingBoundary,fourierCoefs,P,T,epsilon,domain, ...
        blockSize),[0,nBeats],p0);
    
else
    if isempty(varargin{1})
        [t,p] = ode45(@(T,P) EvaluateVelocityFromFourier(stationaryBoundary, ...
            movingBoundary,fourierCoefs,P,T,epsilon,domain, ...
            blockSize),[0,nBeats],varargin{2});
    else
        [t,p] = ode45(@(T,P) EvaluateVelocityFromFourier(stationaryBoundary, ...
            movingBoundary,fourierCoefs,P,T,epsilon,domain, ...
            blockSize),[0,nBeats],varargin{2}(end,:));
        p = [varargin{2} ; p(2:end,:)];
        t = [varargin{1} ; t(2:end) + varargin{1}(end)];
    end
end

end
