%SIMULATEUNICILIATEDNODE SIMULATES PARTICLE TRACKING FOR THE UNICILIATED
%MOUSE NODE
function SimulateUniciliatedNode

%% Initialise cilia at [0,0]
x0 = [0,0];
nC = size(x0,1);
forceFileName = 'data/forces-mutant-1C.mat';

%% Give option to recalculate forces
forceChoice = input(sprintf(['What do you want to do?\n' ...
    '\t1:\t Load force data from paper\n' ...
    '\t2:\t Simulate force data\n']...
    ));

while forceChoice ~= 1 && forceChoice ~= 2
    fprintf('\nPlease choose from available options\n')
    
    forceChoice = input(sprintf(['What do you want to do?\n' ...
        '\t1:\t Load force data from paper\n' ...
        '\t2:\t Simulate force data\n']...
        ));
end

if forceChoice == 2
    CalculateForces(x0,forceFileName);
end

%% Load forces
load(forceFileName,'fC','stationaryBoundary','movingBoundary', ...
    'epsilon','domain','blockSize')

%% Get number of beats 
nParticles = input('Number of particle paths to simulate:');
nBeatsMax = input('Number of cilium beats to simulate:');

%% Get particle release location
releaseLoc = input(sprintf(['Where do you want to release particles?\n' ...
    '\t1:\t Uniform release\n' ...
    '\t2:\t Near-cilium release\n']...
    ));

while releaseLoc ~= 1 && releaseLoc ~= 2 && releaseLoc ~= 3
    fprintf('\nPlease choose from available options\n')
    
    releaseLoc = input(sprintf(['What do you want to do?\n' ...
        '\t1:\t Load force data from paper\n' ...
        '\t2:\t Simulate force data\n']...
        ));
end

switch releaseLoc
    case 1
        relLoc = 'uniform';
        saveDir = 'data/paths-mutant-1C-uniform';
    case 2
        relLoc = 'near-cilium';
        saveDir = 'data/paths-mutant-1C-near-cilium';
end

%% Simulate
fprintf('\n\tSimulating %i particles for %i beats...',nParticles,nBeatsMax)
pause(0.001)

saveName = sprintf('paths-mutant-1C_nParticles%i_nBeats%i.mat', ...
    nParticles,nBeatsMax);

SimulateParticlePath(fC,stationaryBoundary, ...
    movingBoundary,nBeatsMax,1,epsilon,nParticles, ...
    domain,blockSize,saveName,saveDir,relLoc);

fprintf('\n\t\tSimulation complete, saved in data/\n')

end
