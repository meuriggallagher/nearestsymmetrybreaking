function SimulateParticlePath(fC,stationaryBoundary, ...
    movingBoundary,nBeatsPerRun,nRuns,epsilon,nParticles, ...
    domain,blockSize,saveName,saveDir,releaseLoc)

animal = 'mouse';

particleModel.nP = nParticles;
particleModel.kB = 1.38064852e-23 * 4e10;
particleModel.T = 273.15 + 37;
particleModel.mu = 0.001 * 4e10;
particleModel.a = 50e-9 * 2e5; % Length of one cilium

%% Parameters for run
dt = 1e-2;
t = 0 : dt : 1;
nT = length(t);

tSave = t;
for ii = 1 : nBeatsPerRun - 1
    tSave = [tSave(:) ; t(2:end)' + t(end)*ii];
end

releaseHeight = 0.2; % Height to release particles at
depositionHeight = 0.1; % Height to consider deposited

%% Release particles from just above base
if strcmpi(releaseLoc,'uniform')
    % Randomly choose points in x and y
    switch animal
        case 'mouse'
            p0 = InitialisePointInMouseNode(particleModel.nP);
        otherwise
            error('Animal not defined for path')
    end
    
    p0(:,3) = releaseHeight + GetBoundaryPoint(p0, movingBoundary.model(1));
    particleModel.p0 = p0;
    
elseif strcmpi(releaseLoc,'near-cilium')
    iCilium = randi([1,movingBoundary.nCilia],1);
    
    x = NaN(nT+1,3);
    for ii = 1 : nT
        % Pick single cilium
        dum = [];
        dum.fn = movingBoundary.fn;
        dum.model = movingBoundary.model(iCilium);
        dum.nCilia = 1;
        dum.runType = movingBoundary.runType;
        
        X = dum.fn(t(ii),dum);
        [x1,x2,x3] = ExtractComponents(X);
        
        x(ii,:) = [x1(end),x2(end),x3(end)];
        
    end
    x(end,1) = dum.model.x0(1);
    x(end,2) = dum.model.x0(2);
    
    % Release particle randomly from points within a box around cilium
    x0 = min(x(:,1)) - 0.2;
    x1 = max(x(:,1)) + 0.2;
    y0 = min(x(:,2)) - 0.2;
    y1 = max(x(:,2)) + 0.2;
    
    % Release point randomly within this box
    px = x0 + (x1-x0)*rand(nParticles,1);
    py = y0 + (y1-y0)*rand(nParticles,1);
    pz = releaseHeight + GetBoundaryPoint([px(:),py(:),nan(nParticles,1)], ...
        dum.model);
    
    % Save
    particleModel.nP = nParticles;
    particleModel.p0 = [px(:),py(:),pz(:)];
    
elseif strcmpi(releaseLoc,'cilium')
    s = 0.2 + 0.8*rand(1);
    tRelease = randi(nT-1,1);
    
    % Get start point for particle
    x = movingBoundary.fn(t(tRelease),movingBoundary);
    [x1,x2,x3] = ExtractComponents(x);
    
    X1 = x1(1 : movingBoundary.model(1).nS);
    X2 = x2(1 : movingBoundary.model(1).nS);
    X3 = x3(1 : movingBoundary.model(1).nS);
    x10 = spline(linspace(0,1,movingBoundary.model(1).nS),X1,s);
    x20 = spline(linspace(0,1,movingBoundary.model(1).nS),X2,s);
    x30 = spline(linspace(0,1,movingBoundary.model(1).nS),X3,s);
    
    X1 = x1(1 + movingBoundary.model(1).nS : 2*movingBoundary.model(1).nS);
    X2 = x2(1 + movingBoundary.model(1).nS : 2*movingBoundary.model(1).nS);
    X3 = x3(1 + movingBoundary.model(1).nS : 2*movingBoundary.model(1).nS);
    x11 = spline(linspace(0,1,movingBoundary.model(1).nS),X1,s);
    x21 = spline(linspace(0,1,movingBoundary.model(1).nS),X2,s);
    x31 = spline(linspace(0,1,movingBoundary.model(1).nS),X3,s);
    
    particleModel.p0 = [x10,x20,x30 ; x11,x21,x31];
    
    % Save
    particleModel.nP = size(particleModel.p0,1);
end

%%
[trackTime,trackPos] = DepositSingleParticle( ...
    particleModel,t,fC,movingBoundary,stationaryBoundary,nBeatsPerRun, ...
    epsilon,domain,blockSize,tSave,depositionHeight,nRuns);

%% Save output
particleTracks = cell(1,2);
particleTracks{1,1} = trackTime;
particleTracks{1,2} = trackPos;
ParSaveTrack(saveName,saveDir,particleTracks);

end
