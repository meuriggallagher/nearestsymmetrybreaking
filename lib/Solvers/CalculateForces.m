function CalculateForces(x0,saveName)

%% Set up calculations
blockSize = 0.2;
epsilon = 0.1/5;
nC = size(x0,1);

runType = 'hybridStokeslets';
domain = 'i';

%% Set up time
nT = 32;
t = linspace(0,1,nT);

%% Parameters
nS = 256;

nFTop = 2000;
nFBot = 4000;
pf = 5e-2;

% testing only
nS = 8;
nFTop = 2;
nFBot = 1;

% Take quadrature points to be 8 * as many as force points
h = [nFTop,8*nFTop,nFBot,8*nFBot,pf,pf/8,nS];

%%
nPp = 1;
if nPp > 1
    parpool(nPp);
end

% Construct mouse node
[stationaryBoundary,movingBoundary] = GenerateMouseNode(h, ...
    nC,x0,blockSize,runType);

% Solve force problem
tic; 
fprintf('\nSolving forces...\n')

nFC = 9;
fC = SolveMovingBoundaryForces(t,movingBoundary, ...
    stationaryBoundary,epsilon,nFC,domain,nPp,blockSize);
fprintf('done\n')
runTime = toc;

% Save
save(saveName,'stationaryBoundary','movingBoundary','fC','t', ...
    'epsilon','blockSize','domain','runTime','-v7.3')

if nPp > 1
    delete(gcp('nocreate'))
end

end
