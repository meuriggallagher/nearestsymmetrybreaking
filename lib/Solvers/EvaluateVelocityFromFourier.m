function u = EvaluateVelocityFromFourier(statBoundary, ...
    movingBoundary,fourierCoefs,xf,t,epsilon,domain,blockSize)

%global outputCounter 
% input:
% xf - field points
% X  - quadrature grid
% x  - force grid
% f  - forces
% NN - nearest-neighbour matrix
%
% output:
% u  - velocity field at xf
switch statBoundary.runType
    case 'hybridStokeslets'
        % Moving boundary points
        [xMoving,~,Rmoving] = movingBoundary.fn(t,movingBoundary);
        
        % Point-surface interactions
        Aps = AssembleStokesletMatrix(xf, statBoundary.X, ...
            statBoundary.x,epsilon,domain,blockSize,statBoundary.NN);
        %Aps = AssembleStokesletMatrixMex_mex(xf, statBoundary.X, ...
        %   statBoundary.x,epsilon,domain,blockSize,statBoundary.NN);        
        
        % Point-line interactions
        Apl=RegStokesletAnalyticIntegrals(xf,xMoving, ...
            1/2/movingBoundary.model(1).nS, Rmoving, epsilon);
        
        % Assemble matrix
        A = [Aps,Apl];
        
    case 'nearestNeighbour'
        % Calculate boundary points
        [xMoving,~,XMoving] = movingBoundary.fn(t,movingBoundary);
        
        % Combine boundaries
        x=MergeVectorGrids(statBoundary.x,xMoving);
        X=MergeVectorGrids(statBoundary.X,XMoving);
        
        % Assemble stokeslet matrix
        A=AssembleStokesletMatrix(xf,X,x,epsilon,domain, ...
            blockSize,statBoundary.NN);
end

% Calculate forces
f = CalculateForceFromFourierCoef(fourierCoefs,t);

% Calculate velocity
u=A*f(:);

% Output time
%disp(t)
%pause(0.001)
% if t > outputCounter 
%     %fprintf('\tBeat %i completed\n',outputCounter)
%     disp(sprintf('\tBeat %i completed\n',outputCounter)) %#ok<DSPS>
%     outputCounter = outputCounter + 1;
%     pause(0.001)
% end
end
