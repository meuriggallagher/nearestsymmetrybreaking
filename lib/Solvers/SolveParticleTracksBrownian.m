function p = SolveParticleTracksBrownian(t,fourierCoefs,movingBoundary, ...
    stationaryBoundary,particleModel,nBeats,epsilon,domain,blockSize)

% Time steps
dT = t(2)-t(1);
nT = length(t);

% Standard deviation for Brownian motion
sd = ( 2 * particleModel.kB * particleModel.T * dT ...
    / (6 * pi * particleModel.mu * particleModel.a) ) ^ 0.5;

% Initialise particle locations
nP = length(particleModel.p0(:));
p = NaN(nP, nBeats * nT);
p(:,1) = particleModel.p0(:);

%%
for iT = 2 : nBeats * nT
    time = t(mod(iT-1,nT) +1);    
    
    % Evaluate hydrodynamic velocity
    u = EvaluateVelocityFromFourier(stationaryBoundary, ...
        movingBoundary,fourierCoefs,p(:,iT-1),time,epsilon,domain, ...
        blockSize);
    
    % Update position by forward Euler with Brownian motion
    p(:,iT) = p(:,iT-1) + dT * u(:) + sd * randn(nP,1);
end

end
