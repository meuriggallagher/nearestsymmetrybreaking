function [trackTime,trackPos] = DepositSingleParticle( ...
    particleModel,t,fC,movingBoundary,stationaryBoundary,nBeatsPerRun, ...
    epsilon,domain,blockSize,tSave,depositionHeight,maxIterations,varargin)

isDeposited = 0;
nIterations = 0;

if isempty(varargin)
    trackPos = particleModel.p0;
    trackTime = 0;
else
    trackPos = varargin{1};
    trackTime = varargin{2};
end

while isDeposited == 0
    tic
    
    % Start point for particle
    particleModel.p0 = trackPos(end,:);
    startTime = trackTime(end);
    
    % Solve for nBeatsPerRun
    p = SolveParticleTracksBrownian(t,fC,movingBoundary, ...
        stationaryBoundary,particleModel,nBeatsPerRun, ...
        epsilon,domain,blockSize);
    
    tParticle = tSave(:) + startTime;
    
    % Calculate base at [p1,p2]
    px = p(1,:)'; py = p(2,:)'; pz = p(3,:)';
    zz = GetBoundaryPoint([px,py,nan(length(pz),1)], ...
        movingBoundary.model(1));
    
    % Calculate points above zz + depositionHeight
    keepParticle = pz > (zz + depositionHeight);
    iRmv = 1 : length(keepParticle);
    iRmv(keepParticle == 1) = [];
    
    if ~isempty(iRmv)
        iKeep = iRmv(1)-1;
        isDeposited = 1;
    else
        iKeep = length(tParticle);
    end
    
    if iKeep > 0
        
        tNew = tParticle(1 : iKeep);
        pNew = [px(1:iKeep) , py(1:iKeep) , pz(1:iKeep)];
        
        % Save track and times
        trackTime = [trackTime ; tNew]; %#ok<AGROW>
        trackPos = [trackPos ; pNew]; %#ok<AGROW>
        
        nIterations = nIterations + 1;
        
        if nIterations >= maxIterations
            isDeposited = 1;
        end
        
        runTime = toc;
        fprintf('\t%i: height: %.4f, time %.2f seconds\n', ...
            nIterations*nBeatsPerRun,pNew(end,3) - zz(iKeep), runTime)
    else
        runTime = toc;
        fprintf('\t%i: time %.2f seconds\n', ...
            nIterations*nBeatsPerRun, runTime)
    end
end

end
