%SIMULATEEUCILIATEDNODE SIMULATES PARTICLE TRACKING FOR THE EUCILIATED
%MOUSE NODE
function SimulateEuciliatedNode

%% Initialise cilia
load('fullMouseCiliaPositions.mat','X0');
x0 = X0;
forceFileName = 'data/forces-mutant-1C.mat';

%% Give option to recalculate forces
forceChoice = input(sprintf(['What do you want to do?\n' ...
    '\t1:\t Load force data from paper\n' ...
    '\t2:\t Simulate force data\n']...
    ));

while forceChoice ~= 1 && forceChoice ~= 2
    fprintf('\nPlease choose from available options\n')
    
    forceChoice = input(sprintf(['What do you want to do?\n' ...
        '\t1:\t Load force data from paper\n' ...
        '\t2:\t Simulate force data\n']...
        ));
end

if forceChoice == 2
    CalculateForces(x0,forceFileName);
end

%% Load forces
load(forceFileName,'fC','stationaryBoundary','movingBoundary', ...
    'epsilon','domain','blockSize')

%% Get number of beats
nParticles = input('Number of particle paths to simulate:');
nBeatsMax = input('Number of cilium beats to simulate:');

%% Get particle release location
relLoc = 'uniform';
saveDir = 'data/paths-wildtype-112C-uniform';

%% Simulate
fprintf('\n\tSimulating %i particles for %i beats...',nParticles,nBeatsMax)
pause(0.001)

saveName = sprintf('paths-wildtype-112C_nParticles%i_nBeats%i.mat', ...
    nParticles,nBeatsMax);

SimulateParticlePath(fC,stationaryBoundary, ...
    movingBoundary,nBeatsMax,1,epsilon,nParticles, ...
    domain,blockSize,saveName,saveDir,relLoc);

fprintf('\n\t\tSimulation complete, saved in data/\n')

end
