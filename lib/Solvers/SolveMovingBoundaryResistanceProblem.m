function f = SolveMovingBoundaryResistanceProblem(movingBoundary, ...
    statBoundary,t,epsilon,domain,blockSize)

% solves force-free multiple swimmer problem for translational and angular
% velocity U, Om in the presence of a stationary rigid boundary
% (e.g. finite plane wall)
%
% input: swimmer{.} - array of structures describing how to construct
%                       swimmer - all same size
%                       (internal variable Nsw is length of swimmer, i.e.
%                       number of swimmers, Ns is no. force points on
%                       swimmer)
%        z       - position/orientation of swimmer
%        dz(1:3*Nsw)        = x0     - origin of swimmer
%        dz(3*Nsw+1:6*Nsw)  = b1     - first basis vector of swimmer frame
%        dz(6*Nsw+1:9*Nsw)  = b2     - second basis vector of swimmer frame
%        t       - time (scalar)
%        epsilon - regularisation parameter
%        domain  - 'i' for no image systems, 'h' for Ainley/Blake halfspace
%        blockSize - memory size to use for stokeslet matrix construction
%                       in GB. 0.2 works for any reasonable hardware
%        varargin  - if varargin{1}='f' then include force components
%
% variables: U      - translational velocity
%            Om     - angular velocity
%            xb     - discretisation of stationary boundary - force points
%            Xb     - discretisation of stationary boundary - quadrature points
%            Is{n}  - index of first point of swimmer n
%
% output: dz        - rate of change of position-orientation (x,b1,b2)
%                     first 3*Nsw compts are dx0/dt,
%                     next  3*Nsw are db1/dt,
%                     next  3*Nsw are db2/dt
%                     each are ordered by 1-components first, then
%                       2-components, then 3-components
%                   - if varargin{1}='f' then includes force components
switch statBoundary.runType
    case 'hybridStokeslets'
           
        % Moving boundary points
        [xMoving,vMoving,Rmoving] = movingBoundary.fn(t,movingBoundary);
        
        % Surface - surface interactions
        Ass = AssembleStokesletMatrix(statBoundary.x, statBoundary.X, ...
            statBoundary.x,epsilon,domain,blockSize,statBoundary.NN);
        
        % Line - surface interactions
        Als = AssembleStokesletMatrix(xMoving, statBoundary.X, ...
            statBoundary.x,epsilon,domain,blockSize,statBoundary.NN);
        
        % Surface - line interactions
        Asl=RegStokesletAnalyticIntegrals(statBoundary.x,xMoving, ...
            1/2/movingBoundary.model(1).nS, Rmoving, epsilon);
        
        % Line - line interactions
        All=RegStokesletAnalyticIntegrals(xMoving,xMoving, ...
            1/2/movingBoundary.model(1).nS, Rmoving, epsilon);
        
        % Assemble resistance problem
        A = [Ass, Asl ; Als, All];
        % v = MergeVectorGrids(vStationary,vMoving);
        v = [0*statBoundary.x ; vMoving];
        
    case 'nearestNeighbour'

        % Moving boundary points
        [xbMoving,vbMoving,XbMoving] = movingBoundary.fn(t,movingBoundary);
        
        % Combine boundaries
        x=MergeVectorGrids(statBoundary.x,xbMoving);
        X=MergeVectorGrids(statBoundary.X,XbMoving);
        v=MergeVectorGrids(0 * statBoundary.x,vbMoving);
        
        % Formulate resistance problem
        A=AssembleStokesletMatrix(x,X,x,epsilon,domain, ...
            blockSize,statBoundary.NN);
        
end

% solve and extract f
f=A\v;

end