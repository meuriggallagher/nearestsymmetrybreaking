function [x,v,rotMat] = RotateCilium(x,v,rotMat,n)

% Calculate rotation matrix from [0,0,1] to n
n1 = cross([0,0,1],n);
c = dot(n,[0,0,1]);
n1X = [0, -n1(3),  n1(2) ; ...
    n1(3),     0, -n1(1) ; ...
    -n1(2),  n1(1),     0 ];
R = eye(3) + n1X + n1X^2*( 1 / (1+c) );

% Rotate cilium
x = R * x'; 
v = R * v'; 
rotMat = R * rotMat';

x = x';
v = v';
rotMat = rotMat';

end
