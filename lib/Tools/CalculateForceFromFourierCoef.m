function f = CalculateForceFromFourierCoef(fourierCoefs,t)

% Number of fourier coefs and force points
[nC,nF] = size(fourierCoefs.aN);

% Number of time points
nT = length(t);

% Calculate fourier series
f = zeros(nT,nF);
for ii = 1 : nC
    f = f + fourierCoefs.aN(ii,:) .* exp(1i*2*pi*t'*fourierCoefs.fN(ii,:));
end
f = real(f);

end
