%MAIN GENERATES ALL FIGURES FOR THE MANUSCRIPT GALLAGHER,
%MONTENGRO-JOHNSON & SMITH 2019, SIMULATIONS OF PARTICLE TRACKING IN THE
%OLIGOCILIATED MOUSE NODE AND IMPLICATIONS FOR LEFT-RIGHT SYMMETRY BREAKING
%MECHANICS
%
% This work is licensed under a Creative Commons Attribution 4.0
% International License.
%
%%
mouseChoice = input(sprintf(['What do you want to do?\n' ...
    '\t1:\t Simulate uniciliated node\n'...
    '\t2:\t Simulate biciliated node\n'...
    '\t3:\t Simulate euciliated node\n']...
    ));

while mouseChoice ~= 1 && mouseChoice ~= 2 ...
        && mouseChoice ~= 3 && mouseChoice ~= 4
    fprintf('\nPlease choose from available options\n')
    
    mouseChoice = input(sprintf(['What do you want to do?\n' ...
        '\t1:\t Simulate uniciliated node\n'...
        '\t2:\t Simulate biciliated node\n'...
        '\t3:\t Simulate euciliated node\n']...
        ));
end

%% Simulate uniciliated node
if mouseChoice == 1
    fprintf('\nSimulating uniciliated node...\n')
    
    % Add path
    p1 = genpath('lib/');
    addpath(p1);
     
    % Simulate node
    SimulateUniciliatedNode;
    
    % Remove path
    rmpath(p1);
    
    fprintf('\n\tComplete.\n\n')
    
end

%% Simulate biciliated node
if mouseChoice == 2
    fprintf('\nSimulating biciliated node...\n')
    
    % Add path
    p1 = genpath('lib/');
    addpath(p1);
    
    % Simulate node
    SimulateBiciliatedNode;
    
    % Remove path
    rmpath(p1);
    
    fprintf('\n\tComplete.\n\n')
    
end

%% Simulate euciliated node
if mouseChoice == 3
    fprintf('\nSimulating biciliated node...\n')
    
    % Add path
    p1 = genpath('lib/');
    addpath(p1);
    
    % Simulate node
    SimulateEuciliatedNode;
    
    % Remove path
    rmpath(p1);
    
    fprintf('\n\tComplete.\n\n')
    
end

end
